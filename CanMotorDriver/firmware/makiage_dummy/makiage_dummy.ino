void setup() {
  // put your setup code here, to run once:
  pinMode(STAT_LED1, OUTPUT);
}

void loop() {
  // put your main code here, to run repeatedly:
  digitalWrite(STAT_LED1, !digitalRead(STAT_LED1));
  delay(1000);
}
