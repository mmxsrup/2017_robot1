/* ? ms ごとに呼び出す関数を登録 */

/*
 * SendToServer()  := state1 と wd4 オブジェクトの値をサーバーに送る
 * UpdateDisplay() := state と wd4 オブジェクトの値の一部を クライアントに表示
 * checkGamepads() := gamepadの値を読み取る
 */


$(document).ready(function() {
	setInterval("SendToServerforwd4()", 100);
	setInterval("SendToServerforstate1()", 100);
	setInterval("UpdateDisplay()", 100);
	setInterval("checkGamepads()", 100);
})