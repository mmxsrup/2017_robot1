/*
"arm1" : {
	"motor"	: {
		"speed"	: -127 to 127
	},
	"servo"	: {
		"top"	: -90 to 90,
		"middle": -90 to 90,
		"bottom": -90 to 90
	},
	"camera" : {
		"top"	: -90 to 90,
		"bottom": -90 to 90
	},
	"reset"	: 0 or 1
}
*/

var state1 = {
	"arm1" : {
		"motor"	: {
			"speed"	: 0,
		},
		"servo"	: {
			"top"	: 0,
			"middle": 0,
			"bottom": 0,
		},
		"camera" : {
			"top"	: 0,
			"bottom": 0,
		},
		"reset"	: 0,
	},
}

function SendToServerforstate1() {
    // console.log("sendarm1", state1);
    ws.send(JSON.stringify(
        state1,
    ));
}

$(document).ready(function() {

	$("#arm1_motor_speed_btn").click(function() {
       	var val = parseInt($("#arm1_motor_speed_val").val(), 10);
        state1.arm1.motor.speed = val;
        console.warn("arm1 motor speed", state1.arm1.motor.speed);
        state1.arm1.reset = 1;
        ws.send(JSON.stringify(
            state1
        ));
    })
    $("#arm1_servo_top_btn").click(function() {
    	var val = parseInt($("#arm1_servo_top_val").val(), 10);
        state1.arm1.servo.top = val;
        console.warn("arm1 servo top", state1.arm1.servo.top);
        state1.arm1.reset = 1;
        ws.send(JSON.stringify(
            state1
        ));
    })
    $("#arm1_servo_middle_btn").click(function() {
    	var val = parseInt($("#arm1_servo_middle_val").val(), 10);
        state1.arm1.servo.middle = val;
        console.warn("arm1 servo middle", state1.arm1.servo.middle);
        state1.arm1.reset = 1;
        ws.send(JSON.stringify(
            state1
        ));
    })
    $("#arm1_servo_bottom_btn").click(function() {
    	var val = parseInt($("#arm1_servo_bottom_val").val(), 10);
        state1.arm1.servo.bottom = val;
        console.warn("arm1 servo buttom", state1.arm1.servo.bottom);
        state1.arm1.reset = 1;
        ws.send(JSON.stringify(
            state1
        ));
    })
    $("#arm1_camera_top_btn").click(function() {
    	var val = parseInt($("#arm1_camera_top_val").val(), 10);
        state1.arm1.camera.top = val;
        console.warn("arm1 camera top", state1.arm1.camera.top);
        state1.arm1.reset = 1;
        ws.send(JSON.stringify(
            state1
        ));
    })
    $("#arm1_camera_bottom_btn").click(function() {
    	var val = parseInt($("#arm1_camera_bottom_val").val(), 10);
        state1.arm1.camera.bottom = val;
        console.warn("arm1 camera bottom", state1.arm1.camera.bottom);
        state1.arm1.reset = 1;
        ws.send(JSON.stringify(
            state1
        ));
    })
    $("#arm1_reset_btn").click(function() {
        state1.arm1.reset = 0;
        console.warn("arm1 reset");
        ws.send(JSON.stringify(
            state1
        ));
    })

})
