function UpdateDisplay() {

    $("#4wdStop").html(wd4["4wd"].stop);
    $("#4wdFree").html(wd4["4wd"].free);
    $("#moveR").html(wd4["4wd"].go.right);
    $("#moveL").html(wd4["4wd"].go.left);

    $("#dmotor").html(state1.arm1.motor.speed);
    $("#dservo_top").html(state1.arm1.servo.top);
    $("#dservo_middle").html(state1.arm1.servo.middle);
    $("#dservo_bottom").html(state1.arm1.servo.bottom);
    $("#dcamera_top").html(state1.arm1.camera.top);
    $("#dcamera_bottom").html(state1.arm1.camera.bottom);

    $("#darm2_0").html(state2[0]);
    $("#darm2_1").html(state2[1]);
    $("#darm2_2").html(state2[2]);
    $("#darm2_3").html(state2[3]);
    $("#darm2_4").html(state2[4]);

    if(stopflag) $("#moveok").html("NO!");
    else         $("#moveok").html("YES!");
}

$(window).load(function(){
	for (var key in op) {
		let data =  "<li>" + key + "  :  " + op[key] + "</li>";
		$("#operation").append(data);
	}
});