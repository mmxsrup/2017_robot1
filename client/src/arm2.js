/*
"arm2"	: {
	"servo"	: {
		"num"	: 0 ~,
		"angle"	: -90 to 90
	},
	"reset"	: 0 or 1
}
*/


/* 
 * state2[i] := numがiのangleの値
 */
var state2 = [0, 0, 0, 0, 0];


$("#arm2_servo_btn").click(function() {
   	var num   = parseInt($("#arm2_servo_num_val").val(), 10);
   	var angle = parseInt($("#arm2_servo_angle_val").val(), 10);
   	console.warn("arm2", num, angle);
    ws.send(JSON.stringify({
    	"arm2" : {
    		"servo" : {
    			"num" : num,
    			"angle" : angle,
    		},
    	},
    }));
})

$("#arm2_servo_reset").click(function() {
	console.warn("arm2 reset");
	ws.send(JSON.stringify({
    	"arm2" : {
    		"reset" : false,
    	},
    }));
})
