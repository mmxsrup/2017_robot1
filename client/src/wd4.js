var wd4 = {
    "4wd" : {
        "stop" : false,
        "free" : false,
        "go" : {
            "left" : 0,
            "right" : 0,
        },
    },
}

function SendToServerforwd4() {
    // console.log("send", wd4);
    ws.send(JSON.stringify(
        wd4,
    ));
}

$(document).ready(function() {

    $("#4wd").click(function() {
        rightval = parseInt($("#right4wd").val(), 10);
        leftval  = parseInt($("#left4wd").val(),  10);
        console.log("4wd", rightval, leftval);
        wd4["4wd"].stop     = false;
        wd4["4wd"].free     = false;
        wd4["4wd"].go.left  = rightval;
        wd4["4wd"].go.right = leftval;
        ws.send(JSON.stringify(
            wd4
        )); 
    })
    $("#4wdstop").click(function() {
        console.log("4wdstop");
        wd4["4wd"].stop     = false;
        wd4["4wd"].free     = false;
        wd4["4wd"].go.left  = 0;
        wd4["4wd"].go.right = 0;
        ws.send(JSON.stringify(
            wd4
        )); 
    })
    $("#4wdfree").click(function() {
        console.log("4wdfree");
        wd4["4wd"].stop     = false;
        wd4["4wd"].free     = true;
        wd4["4wd"].go.left  = 0;
        wd4["4wd"].go.right = 0;
        ws.send(JSON.stringify({
            wd4
        }));
    })

})  