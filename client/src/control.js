var ws = new WebSocket(SERVER_URL, "control");


/*
 * コントローラーの Axis の値 をパースして, オブジェクトの値を変える関数
 * arg := Axisの値は入ったオブジェクト
 */
function ParseDataAxes(arg) {
    // console.warn("ParseDataAxes");
    if (stopflag) return true;
    for (var index = 0; index < arg.length; index++) {
        data = arg[index];
        switch (index) {
            case AXIS_MOVE_R : 
                /* 整数値にする必要がある wd4_flagに合わせて 正負を調整 */
                if(WD4_FLAG) wd4["4wd"].go.right = Math.ceil(data *  255.0);
                else         wd4["4wd"].go.right = Math.ceil(data * -255.0);
                break;
            case AXIS_MOVE_L : 
                /* 整数値にする必要がある wd4_flagに合わせて 正負を調整 */
                if(WD4_FLAG) wd4["4wd"].go.left = Math.ceil(data *  255.0);
                else         wd4["4wd"].go.left = Math.ceil(data * -255.0);
                break;
        }
    }
}

/*
 * コントローラの Button の値 をパースして, オブジェクトんも値を変える関数
 * arg := true ならボタンは押されていて, falseなら押されていない
 * index := 何番目のindexのボタンか
 */
function ParseDataButton(arg) {

    // console.warn("ParseDataButton");
    for (var index = 0; index < arg.length; index++) {
        var data  = arg[index].pressed;
        var mflag = arg[BUTTON_MINUS_FLAG].pressed; // trueなら逆向きに動かすボタンが押されている
        if (!data) continue;
        // console.log("ok", index);
        switch (index) {
            case BUTTON_MOVE_STOP :
                if (stopflag) {
                    /* STOP解除 */
                    wd4["4wd"].go.stop = false;
                    stopflag = false;
                } else {
                    /* STOP */
                    wd4["4wd"].go.right = 0, wd4["4wd"].go.left = 0;
                    wd4["4wd"].go.stop = true;
                    stopflag = true;
                }
                break;
            case BUTTON_MOTOR : 
                if (mflag) state1.arm1.motor.speed -= ARM_DIFF;
                else       state1.arm1.motor.speed += ARM_DIFF;
                break;
            case BUTTON_SERVO_TOP :
                if(mflag) state1.arm1.servo.top -= ARM_DIFF;
                else      state1.arm1.servo.top += ARM_DIFF;
                break;
            case BUTTON_SERVO_MIDDLE :
                if(mflag) state1.arm1.servo.middle -= ARM_DIFF;
                else      state1.arm1.servo.middle += ARM_DIFF;
                break;
            case BUTTON_SERVO_BOTTOM :
                if(mflag) state1.arm1.servo.bottom -= ARM_DIFF;
                else      state1.arm1.servo.bottom += ARM_DIFF;
                break;
            case BUTTON_CAM_TOP : 
                if (mflag) state1.arm1.camera.top -= ARM_DIFF;
                else       state1.arm1.camera.top += ARM_DIFF;
                break;
            case BUTTON_CAM_BOTTOM :
                if (mflag) state1.arm1.camera.bottom -= ARM_DIFF;
                else       state1.arm1.camera.bottom += ARM_DIFF;
                break;
            case BUTTON_ARM2_NUM0 :
                sendjsonforArm2(mflag, 0);
                break;
            case BUTTON_ARM2_NUM1 :
                sendjsonforArm2(mflag, 1);
                break;
            case BUTTON_ARM2_NUM2 :
                sendjsonforArm2(mflag, 2);
                break;
            case BUTTON_ARM2_NUM3 :
                sendjsonforArm2(mflag, 3);
                break;
            case BUTTON_ARM2_NUM4 :
                sendjsonforArm2(mflag, 4);
                break;
        }
    }
}

/*
 * arm2は一度jsonを送るだけで良い
 */
function sendjsonforArm2(flag, index) {
    if(flag) state2[index] -= ARM_DIFF;
    else     state2[index] += ARM_DIFF;
    // console.log("arm2send", index, state2[index]);
    ws.send(JSON.stringify({
        "arm2" : {
            "servo" : {
                "num" : index,
                "angle" : state2[index],
            },
        },
    }));
}