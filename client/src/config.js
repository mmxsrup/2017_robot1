/* サーバー(pc) のipアドレスを設定 */
var SERVER_IP = "192.168.21.151:8080";
var SERVER_URL = "ws://" + SERVER_IP;


/*
 * 以下の設定について
 * Gamepad Tester (http://html5gamepad.com/)
 * 上記のサイトを利用し, AxisやButtonのindexどこに割り当てられているかを登録 
 * OSやブラウザの環境依存が激しいためここで設定
 */


/*
 * AXIS_MOVE_R := 右の足回りのAxisの番号
 * AXIS_MOVE_L := 左の足回りのAXisの番号
 */
var AXIS_MOVE_R = 3;
var AXIS_MOVE_L = 1;

/*
 * 足回りSTOP 
 * STOP ボタンを押すと stopflagがトグルされ, 
 */
var BUTTON_MOVE_STOP = 11;
var stopflag = true;

/*
 * joystickを前方へ倒した場合にGamepad Tester(http://html5gamepad.com/) が -1になる時 false
 * そうでなければ true
 */
var WD4_FLAG = false;


/*
 * アーム1を動かすためのボタンを登録
 */
var BUTTON_MOTOR        = 12;
var BUTTON_SERVO_TOP    = 15;
var BUTTON_SERVO_MIDDLE = 13;
var BUTTON_SERVO_BOTTOM = 14;

/*
 * カメラアームを動かすためのボタンを登録
 */
var BUTTON_CAM_TOP    = 8; 
var BUTTON_CAM_BOTTOM = 9;

/*
 * アーム2を動かすためのボタンを登録
 */
var BUTTON_ARM2_NUM0 = 6; 
var BUTTON_ARM2_NUM1 = 7;
var BUTTON_ARM2_NUM2 = 4;
var BUTTON_ARM2_NUM3 = 5;
var BUTTON_ARM2_NUM4 = 0;

/* 
 * 逆方向に動かす時に使うボタン
 */
var BUTTON_MINUS_FLAG = 2;

/*
 * アームを動かすボタンを一度押したらどれだけ動かすかを設定
 */
var ARM_DIFF = 3;

var op = {
	"右の足" : "右のAxis",
	"左の足" : "左のAxis",
	"足STOP" : "右のAxis押し込み",
	"アーム1MOTOR" : "+字 上",
	"アーム1TOP" : "+字 右",
	"アーム1MIDDLE" : "+字 下",
	"アーム1BOTTOM" : "+字 左",
	"カメラアームTOP" : "START",
	"カメラアームBOTTOM" : "SELECT",
	"アーム2NUM0" : "L1",
	"アーム2NUM1" : "R1",
	"アーム2NUM2" : "L2",
	"アーム2NUM3" : "R2",
	"アーム2NUM4" : "△",
	"逆向き" : "✖︎を押しながら",
}