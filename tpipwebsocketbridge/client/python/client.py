#!/usr/bin/env python3
# -*- coding:utf-8 -*-

""" TpipWebsocketBridge Arduinoを使って制御する簡易サンプル

実行する前にサーバーを立ち上げておく必要あり。
実行するとシリアルポートに接続されたデバイスのリストが表示されるので接続するものの番号を入力
シリアルポートから Serial.println(整数); で送られてきたデータを受け取り、TPIPのpwm0に正規化して出力するようデータを送る

"""
__author__ = 'Kenya Ukai'
__version__ = '1'

import websocket
import json
import serial
import serial.tools.list_ports


def main():
    # サーバーに接続
    ws = websocket.create_connection("ws://localhost:8080/", subprotocols = ["control"])

    # シリアルポートから接続するデバイスを選ぶ処理
    list_com_port = list(serial.tools.list_ports.comports())
    print("* Serial Ports *")
    for i, port in enumerate(list_com_port):
        print(i, port.device)
    print("please input number >> ", end = "")
    port_num = int(input())
    
    # シリアルポートオープン
    ser = serial.Serial(list_com_port[port_num].device, 9600)
    print(ser.name)
    
    while True:
        try : 
            # 一応サーバーから受信（しなくてもいい）
            receive_str =  ws.recv()
            receive_data = json.loads(receive_str)
            print("receive : ", receive_data)
            
            # シリアルポートから読み込み
            byte_data = ser.readline()
            str_data = byte_data.decode('utf-8')
            analog_value = int(str_data)
            analog_value = min(1000, analog_value) * 2 - 1000 # [0,1024] -> [-1000,1000]
            
            # サーバーに送信
            send_data = {
                "pwm" : {
                    "0" : analog_value
                }
            }
            print("send : ", send_data)
            ws.send(json.dumps(send_data))
        except KeyboardInterrupt:
            # ここなんか思ってる挙動と違う
            ws.close()
            ser.close()     
            
if __name__ == '__main__':
    main()
