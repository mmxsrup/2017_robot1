#ifndef ROBOT_H
#define ROBOT_H

#include "jansson.h"

#ifdef __cplusplus
extern "C" {
#endif

// TPLX初期化直後に呼ばれる関数 
extern int robot_setup();

// 定期的に呼ばれる関数
extern int robot_loop();
    
// 制御情報を受信した時に呼ばれる関数
// !! rcv_json は裏で勝手に解放される
extern int robot_on_receive(const json_t *rcv_json);

// センサ情報を情報を送信する前に呼ばれる関数
// !! send_json に追加する値は全て所有権を譲渡しないといけない
extern int robot_on_send(json_t *send_json);

// 以下はライブラリ内で定義されている関数
// カスタマイズ部のmainから呼び出す.
extern int server_main();
    
#ifdef __cplusplus
}
#endif

#endif
