/**
* file   : arm.c
* date   : 2016-06-26
*/

#include <stdio.h>
#include <time.h>
#include <math.h>

#include "lib/can.h"

#include "system.hpp"


bool parse_arm2(json_t *json) {
	static uint8_t num;
	static int8_t angle;

	json_t *servo = json_object_get(json, "servo");
	if (json_is_object(servo)) {
		json_t *servo_num = json_object_get(servo, "num");
		json_t *servo_angle = json_object_get(servo, "angle");

		if (json_is_number(servo_num))
			num = json_number_value(servo_num);

		auto normalize = [](double a) -> int8_t {
			if (a > 90) return 90;
			else if (a < -90) return -90;
			else return a;
		};

		if (json_is_number(servo_angle)) angle = normalize(json_number_value(servo_angle));

		can_send_buffer.update(
			(uint8_t)CAN_ID::ARM2,
			(uint8_t)CAN_COMMANDS_ARM2::SERVO,
			(uint8_t)num,
			(int8_t)angle);
		std::cout << "num:angle " << (int)num << ' ' << (int)angle << std::endl;
		return true;
	}

	json_t *reset = json_object_get(json, "reset");
	if (json_is_boolean(reset)) {
		uint8_t off = json_boolean_value(reset);
		can_send_buffer.update(
			(uint8_t)CAN_ID::ARM2,
			(uint8_t)CAN_COMMANDS_ARM2::RESET, off);
		std::cout << "reset" << std::endl;
		return true;
	}
	return false;
}
