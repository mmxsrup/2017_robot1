#ifndef SYSTEM_HPP
#define SYSTEM_HPP

#include <stdint.h>
#include <cstring>
#include <cassert>
#include <map>
#include <iostream>

#include "jansson.h"
#include "tpip.h"
#include "lib/can.h"
#include "lib/buffer.hpp"

/*
 * ボード定義
 */

enum class CAN_ID : uint8_t {
    BROAD_CAST ,
    TPIP ,
    WHEEL_MASTER,
    WHEEL_SLAVE,
    ARM1_MOTOR,
    ARM1_SERVO,
	ARM2,
    RESCUE_BOARD,
    CAN_BOARD_NUM
};

static_assert((int)CAN_ID::CAN_BOARD_NUM <= 15, "CAN BOARD ID should be less than 15");


enum class CAN_COMMANDS_4WD : uint8_t {
    RESET,
    MOVE,
    STOP,
    FREE
};


extern bool parse_4wd(json_t *json);


/*
 * レスキューボードコマンド
 */
 

enum class CAN_COMMANDS_RESCUE_BOARD : uint8_t {
    GO = 1,
    ROLL
};

/*
 * アーム1コマンド
 *
	for アーム1
	arm1 : {
		motor : {
			speed	: -127 to 127
		},
		servo : {
			top		: -90 to 90,
			middle	: -90 to 90,
			bottom	: -90 to 90
		},
		camera : {
			top		: -90 to 90,
			bottom	: -90 to 90
		},
		reset : 0 or 1
	}
 */

enum class CAN_COMMANDS_ARM1_MOTOR : uint8_t {
	MOTOR = 1
};

enum class CAN_COMMANDS_ARM1_SERVO : uint8_t {
	RESET = 0,
	ARM = 1,
	CAMERA = 2
};

/*
 * アーム2コマンド
 *
	for アーム2
	arm2 : {
		servo : {
			num		: 0 ~,
			angle	: -90 to 90
		},
		reset : 0 or 1
	}
*/

enum class CAN_COMMANDS_ARM2 : uint8_t {
	RESET,
	SERVO
};

extern bool parse_arm1(json_t *json);

extern bool parse_arm2(json_t *json);

extern SendBuffer<(uint8_t)CAN_ID::TPIP> can_send_buffer;

#endif
