[ホーム](readme.md) > APIリファレンス

# APIリファレンス

httpリクエスト等でやっても良かったが、cで書いていてhttpサーバー別に加えるのが面倒だったのと、他のメンバーの意見もあって全部Websocket

通信はすべて文字列化したJSONで行う(JSON.stringifyする)

以下は最新でない可能性があります

---

## control

TPIPのIOを制御するサブプロトコル
 	
以下は標準仕様であり、カスタマイズによって追加・削除可能
 	
### IN
	
送られてきたJSONに含まれるキーだけが設定され、含まれない部分は変化しない

また、関係のないキーは全て無視される	
		
| キー | 型 | 詳細 |
|:-----------|------------:|:------------:|
| get_control | bool | trueで送ると、現在の制御出力状態を格納したjsonを１度だけ送り返す。 |
| get_config | bool | trueで送ると、サーバーの設定等を格納したjsonを１度だけ送り返す。 |
| streaming     | bool | センサー値の定期的にストリーミングをするかどうかの設定 |
| fps | int | センサー値のストリーミング頻度（目標値）を設定する |
| digital | 配列 [bool, bool,...] | 配列のインデックスに対応するデジタル出力をそれぞれ設定する。（デジタル出力0は配列の0番目の要素の値になる） |
| digital | object {key : bool} | キーとして"0", "1", ... , を使うと、対応するデジタル出力が指定値になる。例）digital["0"] = true で デジタル出力0をHigh |
| pwm   | 配列 [int, int,...]  | PWM出力を設定する。詳細はdigitalと同じ |
| pwm   | object {key : int}  | PWM出力を設定する。詳細はdigitalと同じ |
| motor   | object {key : int}  | モーター出力を設定する。詳細はdigitalと同じ。TPIP3にはモーター出力がないので、設定しても無駄なことに注意。 |
| motor   | 配列 [int, int,...]  | モーター出力を設定する。詳細はdigitalと同じ |
	
	
### OUT
	
streaming : true（デフォルト） の時、定期的(上で指定したfps)に送られる			
	
| キー | 型 | 詳細 |
|:-----------|------------:|:------------:|
| digital | 配列 [bool, bool, ...] | 配列のインデックスに対応するデジタル入力がHighかどうか |
| analog   | 配列 [real, real, ...]  | 配列のインデックスに対応するアナログ入力を 0 ~ 100に正規化した値 |
| pulse   | 配列 [int, int, ...]  | 配列のインデックスに対応するパルス入力の値 |
| battery   | real  | TPIPに接続されているバッテリーの電圧(V) |
| connection.tpip   | bool | サーバーがTPIP画像ボードと接続確立しているか |
| connection.control   | bool | サーバーがTPIP制御ボードと接続確立しているか |
	
	
get_control: true を送った時

INと同じデータ（配列・Object両方受け入れるものは配列のみ）
	
get_config: true を送った時
	
| キー | 型 | 詳細 |
|:-----------|------------:|:------------:|
| target | string | サーバーが接続するTPIPのIPアドレス |	

## camera-stream

カメラ映像ストリーミング用
	
### IN
	
| キー | 型 | 詳細 |
|:-----------|------------:|:------------:|
| get | bool | trueで送ると、現在の各種設定値を格納したjsonを返す。１回の送信につき、１回の返答。	 
| streaming     | bool | カメラ画像のストリーミングをするかどうかの設定 |
| fps | int | ストリーミング速度を設定する |
| camera   | int  | カメラ番号を設定する(0 ~ ) |
| size   | string  | 画像の解像度を設定する。"VGA" or "QVGA" |
| kbps | int | TPIPからの画像ストリーミング速度を設定する？ ||
 
### OUT
	
- jpeg形式のバイナリ画像データを定期的に送る(`binary`形式)
- 要求があれば以下のjsonを文字列として送る(`文字列形式`)

クライアントは、データタイプがバイナリか文字列か見ることで判定可能

| キー | 型 | 詳細 |
|:-----------|------------:|:------------:|
| ip   | string  | 接続（or接続試行）中のTPIPのIP |
| camera   | int  | カメラ番号 |
| kbps | int | TPIPからの画像ストリーミング速度（実測） |
| status   | string  | 接続状態を表す文字列、仕様変更の可能性大 |


## sound

音声ストリーミング用