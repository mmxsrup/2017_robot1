#ifndef SERVER_SOUND_H
#define SERVER_SOUND_H

#include "libwebsockets.h"
#include <stdbool.h>

//
// 音声サーバー
//

#ifdef __cplusplus
extern "C" {
#endif

extern struct lws_protocols server_protcol_sound;


extern bool server_init_sound(struct lws_context *context);
extern bool server_tasks_sound();
extern bool server_destroy_sound();

#ifdef __cplusplus
}
#endif

#endif