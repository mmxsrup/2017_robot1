#ifndef SERVER_HTTP_H
#define SERVER_HTTP_H

#include "libwebsockets.h"
#include <stdbool.h>

#ifdef __cplusplus
extern "C" {
#endif

extern struct lws_protocols server_protcol_http;

extern bool server_init_http(struct lws_context *context, const char *resource_path);
extern bool server_destroy_http();

#ifdef __cplusplus
}
#endif

#endif