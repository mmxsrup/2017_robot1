
#include "config_manager.h"
#include <stdio.h>
#include <stdbool.h>
#include "config.h"

static json_t* config_ = NULL;

static json_t *default_config();
static json_t* load_config(const char *rel_path);
//
//
// public
//
//

bool config_init(const char *rel_path) {
    if(config_ == NULL) {
        config_ = load_config(rel_path);
        json_t *defaults = default_config();
        json_object_update_missing(config_, defaults);
        json_decref(defaults);
    }
    return true;
}

json_t* config_get(const char *key) {
    return json_object_get(config_, key);
}

//
//
// private
//
//

static json_t *default_config() {
    json_t *root = json_object();
    // 接続先TPIPアドレス
    json_object_set_new(root, "ip", json_string("192.168.0.200"));
    // HTTPサーバーで公開するフォルダのパス
    json_object_set_new(root, "public", json_string(SERVER_RESOURCE_DIR));
    // サーバーのポート
    json_object_set_new(root, "port", json_integer(8080));

    return root;
}

// 相対パスでjsonを読み込む
json_t* load_config(const char *rel_path) {
    json_error_t err;
    json_t* ret = json_load_file(rel_path, 0, &err);
    if(ret == NULL) {
        printf("Json Error while loading [ %s ] \n message : %s\n", err.source, err.text);
        printf("line : %d, column : %d\n", err.line, err.column);
        return json_object();
    } else {
        return ret;
    }
}
