

#include "tpip.h"
#include <stdio.h>
#include <time.h>



/*
 * Remote Serial通信受信速度測定実験用
 * 別の機器から連続的にデータを送っておく
 * 1sあたりに受信したバイト数を表示する
 *
 */
void calc_receive_speed() {
	char buf[1024 + 1];
	int size = 0;
	while (true) {
		int sum = 0;
		clock_t start = clock();
		while ((double)(clock() - start) / CLOCKS_PER_SEC < 1.0) {
			for (int i = 0; i < 10; i++) {
				tpip_receive_serial(buf, sizeof(buf), &size);
				if (size > 0) {
					buf[size] = '\0';
					printf("Receive[%d] %s\n", size, buf);
					sum += size;
				}
			}
		}
		clock_t end = clock();
		double time = (double)(end - start) / CLOCKS_PER_SEC;
		printf("Time %f s | Received %d bytes | speed %f bps\n", time, sum, sum / time);
	}
}

/*
 * 適当に送って受信する
 */
void send_and_receive_loop() {
	const char *c = "test\r\n";
	char buf[1024 + 1];
	int size;
	while (true) {
		for (int i = 0; i < 50; i++) tpip_send_serial(c, strlen(c));
		tpip_receive_serial(buf, sizeof(buf), &size);
		if (size > 0) {
			buf[size] = 0;
			printf("Receive [ %d bytes] : %s\n", size, buf);
		}
	}
}


/*
 * 適当に送って受信する
 */
void send_only() {
	char sendbuf[10] = "a hoo\r\n";
	char buf[1024 + 1];
	int size;
	while (true) {
		for (int i = 0; i < 50; i++) {
			tpip_send_serial(sendbuf, strlen(sendbuf));
			if (++sendbuf[0] > 'z') sendbuf[0] = 'a';
		}
		tpip_receive_serial(buf, sizeof(buf), &size);
		if (size > 0) {
			buf[size] = 0;
			printf("Receive [ %d bytes] : %s\n", size, buf);
		}
	}
}


int main() {
	tpip_init("192.168.0.200");
	tpip_connect_control();
	tpip_connect_jpeg();
	while (!tpip_is_control_connected());
	puts("connected");

	tpip_connect_serial("192.168.0.200", 9000);

	//send_and_receive_loop();
	send_only();
	return 0;
}