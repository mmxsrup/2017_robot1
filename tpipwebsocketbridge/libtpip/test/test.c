#include "tpip.h"
#include <stdio.h>
#include <signal.h>
#ifdef _WIN32
#include <windows.h>
#else 
#include <sys/time.h>
#endif

volatile int force_exit = 0;

void sigcatch(int sig) {
	if (sig == SIGINT) {
		force_exit = 1;
	}
}

int main() {
	puts("Start libtpip test.");

	if (tpip_init("192.168.0.200") == TPIP_ERROR) {
		puts("failed to init");
		exit(-1);
	}

	if (tpip_connect_control() == TPIP_ERROR) {
		puts("failed to connect control");
		exit(-1);
	}

	if (tpip_connect_jpeg() == TPIP_ERROR) {
		puts("failed to connect jpeg");
		exit(-1);
	}


    
	printf("target ip : %s\n", tpip_get_target_ip());
	puts("waiting for connection.");

	while (!tpip_is_control_connected() || !tpip_is_jpeg_connected());
	puts("connection ok!");
	
	tpip_set_jpeg_request_speed(1000);
	tpip_set_jpeg_size(TPIP_JPEG_VGA);
	tpip_set_camera_no(1);

    struct tpip_sensor_data sensor;    
	struct tpip_control_data control;
	int cnt = 0;
	while (!force_exit) {
		printf("- - - - - - - - - - - - %d\n", cnt++);
		printf("wifi link quality : %d\n", tpip_get_wlink_quality());
		printf("controlboard connection : %d\n", tpip_is_controlboard_working());
		printf("jpeg request speed : %d\n", tpip_get_jpeg_request_speed());
		printf("jpeg actual speed : %d\n", tpip_get_jpeg_actual_speed());
		printf("jpeg delay time : %d\n", tpip_get_jpeg_delay_time());
		printf("camera no : %d\n", tpip_get_camera_no());
		tpip_get_sensor(&sensor);
		tpip_get_control(&control);


#ifdef _WIN32
		Sleep(1000);
#else 
		usleep(1000 * 1000 * 1000);
#endif
	}
    
    tpip_close_control();
    tpip_close_jpeg();
	tpip_close();
}