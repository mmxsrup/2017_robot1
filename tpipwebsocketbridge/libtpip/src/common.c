#include "tpip.h"
#include <assert.h>
#include <stdlib.h>
#include <string.h>

//
// sound data functions
//

bool tpip_sound_data_reserve(struct tpip_sound_data *data, int size) {
    if(data->allocated >= size) return TPIP_OK;
    return tpip_sound_data_shrink_to_fit(data, size);
}

bool tpip_sound_data_shrink_to_fit(struct tpip_sound_data *data, int size) {
    data->allocated = size;    
    if(data->allocated > 0) {
        data->data = (uint8_t*)realloc(data->data, data->allocated);
    } else {
        data->data = (uint8_t*)malloc(data->allocated);
    }
    if(data->data == NULL) {
        data->allocated = 0;
        return TPIP_ERROR;
    } else {
        return TPIP_OK;
    }
}

bool tpip_sound_data_free(struct tpip_sound_data *data) {
    if(data->allocated > 0) {
        free(data->data);
        data->data = NULL;
    }
    data->allocated = 0;
    data->size = 0;
    return true;
}

//
// sound format convert
//


bool tpip_sound_data_convert_int16_to_float32(struct tpip_sound_data *dest,
                                              const struct tpip_sound_data *src) {
    assert(sizeof(float) == 4);
    assert(sizeof(int16_t) == 2);
    
    int dest_size = sizeof(float);
    int src_size = sizeof(int16_t);
    
    if(src->format != TPIP_SOUND_FORMAT_S16) return false;
    
    tpip_sound_data_reserve(dest, src->size / src_size * dest_size);
    dest->format = TPIP_SOUND_FORMAT_F32;
    dest->sample_rate = src->sample_rate;
    dest->channel_num = src->channel_num;
    dest->size = src->size / src_size * dest_size;
    
    for (int i = 0; i < src->size / src_size; i++) {
        int16_t s;
        memcpy(&s, &src->data[i * src_size], src_size);
        float f = s / 32768.0;
        memcpy(&dest->data[i * dest_size], &f, dest_size);
    }
    return true;
}

bool tpip_sound_data_convert_float32_to_int16(struct tpip_sound_data *dest,
                                              const struct tpip_sound_data *src) {
    assert(sizeof(float) == 4);
    assert(sizeof(int16_t) == 2);

    int src_size = sizeof(float);
    int dest_size = sizeof(int16_t);
    
    if(src->format != TPIP_SOUND_FORMAT_F32) return false;
    
    tpip_sound_data_reserve(dest, src->size / src_size * dest_size);
    dest->format = TPIP_SOUND_FORMAT_S16;
    dest->sample_rate = src->sample_rate;
    dest->channel_num = src->channel_num;
    dest->size = src->size / src_size * dest_size;
    
    for (int i = 0; i < src->size / src_size; i++) {
        float s; memcpy(&s, &src->data[i * src_size], src_size);
        int16_t f = s * 32768.0;
        memcpy(&dest->data[i * dest_size], &f, dest_size);
    }
    return true;
}
