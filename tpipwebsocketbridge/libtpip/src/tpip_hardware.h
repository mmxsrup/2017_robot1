/*
 * TPIP2 or TPIP3 or TPIP_EMULATOR
 *
 * if TPIP3 then option TPIP3_USE_PWM_EXTENSION_BOARD
 *
 */ 

#ifndef TPIP_HARDWARE_H
#define TPIP_HARDWARE_H

#include "tpip_config.h"

#if defined(TPIP2)

#define TPIP_DIGITAL_OUT_COUNT (9)
#define TPIP_DIGITAL_IN_COUNT  (9)
#define TPIP_ANALOG_IN_COUNT   (10)
#define TPIP_PULSE_IN_COUNT    (2)
#define TPIP_PWM_OUT_COUNT     (10)
#define TPIP_MOTOR_COUNT       (1)

#define TPIP_PWM_OUT_MAX ( 1000)
#define TPIP_PWM_OUT_MIN (-1000)
#define TPIP_MOTOR_MAX   ( 1000)
#define TPIP_MOTOR_MIN   (-1000)

#elif defined(TPIP3)

#define TPIP_DIGITAL_OUT_COUNT ( 4)
#define TPIP_DIGITAL_IN_COUNT  ( 4)
#define TPIP_ANALOG_IN_COUNT   ( 8)
#define TPIP_PULSE_IN_COUNT    ( 4)
#define TPIP_MOTOR_COUNT       ( 0)

#ifdef TPIP3_USE_PWM_EXTENSION_BOARD
#define TPIP_PWM_OUT_COUNT ( 20)
#else
#define TPIP_PWM_OUT_COUNT ( 4)
#endif // TPIP3_USE_PWM_EXTENSION_BOARD //
#define TPIP_PWM_OUT_EXTENSION_START ( 5)

#define TPIP_PWM_OUT_MAX (  1200)
#define TPIP_PWM_OUT_MIN ( -1200)
#define TPIP_MOTOR_MAX   ( 0)
#define TPIP_MOTOR_MIN   ( 0)

#elif defined(TPIP_EMULATOR)

#define TPIP_DIGITAL_OUT_COUNT ( 5 )
#define TPIP_DIGITAL_IN_COUNT  ( 5 )
#define TPIP_ANALOG_IN_COUNT   ( 5 )
#define TPIP_PULSE_IN_COUNT    ( 5 )
#define TPIP_PWM_OUT_COUNT     ( 5 )
#define TPIP_MOTOR_COUNT       ( 1 )

#define TPIP_PWM_OUT_MAX (  1000 )
#define TPIP_PWM_OUT_MIN ( -1000 )
#define TPIP_MOTOR_MAX   (  1000 )
#define TPIP_MOTOR_MIN   ( -1000 )

#endif

// （共通）
#define TPIP_ANALOG_IN_MAX ( 65535 )
#define TPIP_ANALOG_IN_MIN ( 0 )
#define TPIP_BATTERY_MAX   ( 3000 )
#define TPIP_BATTERY_MIN   ( 0 )
#define TPIP_PULSE_IN_MAX  ( 32767 )
#define TPIP_PULSE_IN_MIN  ( -32768 )

#endif
