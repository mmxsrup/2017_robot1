/** 
 * @file imu.h
 * @brief IMU communication program Header file
 *
 * @author  Katayama
 * @date    2015-02-02
 * @version 1.00 2015/02/02
 * @version 1.10 2015/03/31 katayama
 *
 * 本プログラムは東京電機大学　工学部機械工学科　栗栖研究室から
 * 提供された情報を元に作成しております。
 */
#ifndef __IMU_H__
#define __IMU_H__

extern int IMU_init(char* port_nm);
extern int IMU_close(void);
extern int IMU_get_data(void);

extern double IMU_get_gyro(int i);
extern double IMU_get_acc(int i);
extern double IMU_get_mag(int i);
extern double IMU_get_quat(int i);

extern void IMU_lock_data(void);
extern void IMU_unlock_data(void);
extern int  IMU_stat(void);
extern int  IMU_reset_gyro(void);

#endif  // #ifndef __IMU_H__

